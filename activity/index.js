const http = require('http');
const port = 8000;

let items = [
	{
		name: "Iphone X",
		price: 3000,
		isActive: true
	},
	{
		name: "Samsung Galaxy S21",
		price: 51000,
		isActive: true
	},
	{
		name: "Razer Blackshark VX2",
		price: 2800,
		isActive: false
	},

];



http.createServer((req, res) => {

	if(req.url === "/" && req.method === "GET"){

		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking GET method');

	} else if(req.url === "/" && req.method === "POST"){

		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is checking a POST method');

	} else if(req.url === "/" && req.method === "PUT"){
		 
		 res.writeHead(200, {'Content-Type' : 'text/ plain'});
		 res.end('This route is for checking a PUT method');

	} else if(req.url === "/" && req.method === "DELETE"){

		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking a DELETE method');

	} else if(req.url === "/items" && req.method === "GET"){

		res.writeHead(200, {'Content-Type' : 'application/json'});
		res.end(JSON.stringify(items));

	}  else if(req.url === "/items" && req.method === "POST"){

		let requestBody = "";

		req.on('data', (data) => {
			requestBody += data
		});


		req.on('end', () => {

			// console.log(requestBody);

			requestBody = JSON.parse(requestBody)

			let newItem = {
				name: requestBody.name,
				price: requestBody.price,
				isActive: requestBody.isActive
			}

			items.push(newItem);

			// console.log(items);

			res.writeHead(200, {'Content-Type' : 'application/json'})
			res.end(JSON.stringify(items))
		})

	} else {
		res.writeHead(404, {'Content-Type' : 'text/plain'})
		res.end('Error 404: Page not available')
	}


}).listen(port);

console.log(`Server is running on localhost:${port}`)